﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Math
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void MatrixTest()
        {

            var m1 = new FTN.MathLibrary.Matrix(3, 3);
            m1.SetValue(0, 0, 2);
            m1.SetValue(0, 1, 10);
            m1.SetValue(0, 2, -4);
            m1.SetValue(1, 0, -1);
            m1.SetValue(1, 1, 4);
            m1.SetValue(1, 2, -3);
            m1.SetValue(2, 0, 5);
            m1.SetValue(2, 1, -6);
            m1.SetValue(2, 2, 0);

            var m2 = new FTN.MathLibrary.Matrix(3, 2);
            m2.SetValue(0, 0, 1);
            m2.SetValue(0, 1, 7);
            m2.SetValue(1, 0, -2);
            m2.SetValue(1, 1, -5);
            m2.SetValue(2, 0, 8);
            m2.SetValue(2, 1, 3);

            var mResult = FTN.MathLibrary.Matrix.Multiply(m1, m2);
        }

        private static void AddTest()
        {
            var matrixA = new FTN.MathLibrary.Matrix(2, 3);
            matrixA.SetRowValues(0, new decimal[] { 1, 3, 1 });
            matrixA.SetRowValues(1, new decimal[] { 1, 0, 0 });

            var matrixB = new FTN.MathLibrary.Matrix(2, 3);
            matrixB.SetRowValues(0, new decimal[] { 0, 0, 5 });
            matrixB.SetRowValues(1, new decimal[] { 7, 5, 0 });


            var result = FTN.MathLibrary.Matrix.Add(matrixA, matrixB);
        }

        private static void TransposeTest()
        {
            var matrixA = new FTN.MathLibrary.Matrix(2, 3);
            matrixA.SetRowValues(0, new decimal[] { 1, 2, 3 });
            matrixA.SetRowValues(1, new decimal[] { 0, -6, 7 });

            var result = FTN.MathLibrary.Matrix.Transpose(matrixA);
        }

        private static void MultiplyTest()
        {
            var matrixA = new FTN.MathLibrary.Matrix(2, 3);
            matrixA.SetRowValues(0, new decimal[] { 2, 3, 4 });
            matrixA.SetRowValues(1, new decimal[] { 1, 0, 0 });

            var matrixB = new FTN.MathLibrary.Matrix(3, 2);
            matrixB.SetRowValues(0, new decimal[] { 0, 1000 });
            matrixB.SetRowValues(1, new decimal[] { 1, 100 });
            matrixB.SetRowValues(2, new decimal[] { 0, 10 });

            var result = FTN.MathLibrary.Matrix.Multiply(matrixA, matrixB);
        }

        private static void DeterminatTest()
        {
            var matrixA = new FTN.MathLibrary.Matrix(3, 3);
            matrixA.SetRowValues(0, new decimal[] { 6, 1, 1 });
            matrixA.SetRowValues(1, new decimal[] { 4, -2, 5 });
            matrixA.SetRowValues(2, new decimal[] { 2, 8, 7 });

            var result = FTN.MathLibrary.Matrix.Determinate(matrixA);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //TransposeTest();
            //MultiplyTest();
            //DeterminatTest();

            HatTest();

            var result = MultipleRegressionTest();

            MessageBox.Show("Done");
        }

        private static void HatTest()
        {
            var knownX = new FTN.MathLibrary.Matrix(11, 3);
            knownX.SetRowValues(0, new decimal[] { 1, 7, 5 }); 
            knownX.SetRowValues(1, new decimal[] { 1, 3, 7 });
            knownX.SetRowValues(2, new decimal[] { 1, 5, 8 });
            knownX.SetRowValues(3, new decimal[] { 1, 8, 1 });
            knownX.SetRowValues(4, new decimal[] { 1, 9, 3 });
            knownX.SetRowValues(5, new decimal[] { 1, 5, 4 });
            knownX.SetRowValues(6, new decimal[] { 1, 4, 0 });
            knownX.SetRowValues(7, new decimal[] { 1, 2, 6 });
            knownX.SetRowValues(8, new decimal[] { 1, 8, 7 });
            knownX.SetRowValues(9, new decimal[] { 1, 6, 4 });
            knownX.SetRowValues(10, new decimal[] { 1, 9, 2 });


            var knownY = new FTN.MathLibrary.Matrix(11, 1);
            knownY.SetRowValues(0, new decimal[] { 65 });
            knownY.SetRowValues(1, new decimal[] { 38 });
            knownY.SetRowValues(2, new decimal[] { 51 });
            knownY.SetRowValues(3, new decimal[] { 38 });
            knownY.SetRowValues(4, new decimal[] { 55 });
            knownY.SetRowValues(5, new decimal[] { 43 });
            knownY.SetRowValues(6, new decimal[] { 25 });
            knownY.SetRowValues(7, new decimal[] { 33 });
            knownY.SetRowValues(8, new decimal[] { 71 });
            knownY.SetRowValues(9, new decimal[] { 51 });
            knownY.SetRowValues(10, new decimal[] { 49 });

            var result = FTN.MathLibrary.Statistics.Hat(knownX);
            var co = FTN.MathLibrary.Statistics.Coefficients(knownX, knownY);

            var reg = FTN.MathLibrary.Statistics.MultipleLeastSquares(knownX, knownY, new[] { "", "Color", "Quality" });

        }

        private static FTN.MathLibrary.LeastSquaresResultSet MultipleRegressionTest()
        {
            var ex2names = new string[] { "", "x1", "x2", "x3", "x4" };
            
            var matrix1 = new FTN.MathLibrary.Matrix(11, 5);
            matrix1.SetRowValues(0, new decimal[] { 1, 2310, 2, 2, 20 });
            matrix1.SetRowValues(1, new decimal[] { 1, 2333, 2, 2, 12 });
            matrix1.SetRowValues(2, new decimal[] { 1, 2356, 3, 1.5m, 33 });
            matrix1.SetRowValues(3, new decimal[] { 1, 2379, 3, 2, 43 });
            matrix1.SetRowValues(4, new decimal[] { 1, 2402, 2, 3, 53 });
            matrix1.SetRowValues(5, new decimal[] { 1, 2425, 4, 2, 23 });
            matrix1.SetRowValues(6, new decimal[] { 1, 2448, 2, 1.5m, 99 });
            matrix1.SetRowValues(7, new decimal[] { 1, 2471, 2, 2, 34 });
            matrix1.SetRowValues(8, new decimal[] { 1, 2494, 3, 3, 23 });
            matrix1.SetRowValues(9, new decimal[] { 1, 2517, 4, 2, 55 });
            matrix1.SetRowValues(10, new decimal[] { 1, 2540, 2, 3, 22 });

            var matrix2 = new FTN.MathLibrary.Matrix(11, 1);
            matrix2.SetValue(0, 0, 142000);
            matrix2.SetValue(1, 0, 144000);
            matrix2.SetValue(2, 0, 151000);
            matrix2.SetValue(3, 0, 150000);
            matrix2.SetValue(4, 0, 139000);
            matrix2.SetValue(5, 0, 169000);
            matrix2.SetValue(6, 0, 126000);
            matrix2.SetValue(7, 0, 142000);
            matrix2.SetValue(8, 0, 163000);
            matrix2.SetValue(9, 0, 169000);
            matrix2.SetValue(10, 0, 149000);

            return FTN.MathLibrary.Statistics.MultipleLeastSquares(matrix1, matrix2, ex2names);
        }
    }
}
