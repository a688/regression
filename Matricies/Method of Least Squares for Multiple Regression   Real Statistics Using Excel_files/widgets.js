<html>
<head>
<title>Web Content Blocked</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<style>
#content{border:3px solid#aaa;background-color:#fff;margin:40;padding:40;font-family:Tahoma,Helvetica,Arial,sans-serif;font-size:15px;}
  h1{font-size:20px;font-weight:bold;color:#c24641;}
  b{font-weight:bold;color:#0B0B61;}
</style>
</head>
<body bgcolor="#e7e8e9">
<div id="content">
<h1>Web Content Blocked</h1>
<br>
<p><font color="151b54">Access to the URL you requested has been blocked in accordance with FHNC policy. Please contact the Help Desk at 901-435-4357 if you feel that the requested URL has been blocked inappropriately. You may also send a screenshot of this entire error page to <a href="mailto:BlueCoat@ftnfinancial.com">BlueCoat@ftnfinancial.com.</a></font></p>
<br>
<br>
<p><b>User ID:</b> first_tennessee\joshua_lonberger </p>
<p><b>URL:</b> platform.twitter.com/widgets.js?_=1396267760329 </p>
<p><b>Category:</b> social-networking </p>
<br>
<br>
<br>
<br>
<div><img src="http://10.225.167.29/index_files/img_logo_FTN_475x65_02.png" width="357" height="49" alt="LOGO - FTN Financial" border="0"></div>
<br>
<p><font color="d8d8d8">PA-2020</font></p>
</div>
<p><font size="2" color="gray"><blockquote>ATTENTION: THE ELECTRONIC COMMUNICATION GUIDELINES HAVE BEEN UPDATED.  AS AN EMPLOYEE OF THIS COMPANY, YOU ARE RESPONSIBLE FOR READING AND UNDERSTANDING THE NEW GUIDELINES.  Taking ownership and responsibility for our actions is an important part of our culture. By logging on to and using this computer, you acknowledge that you have read, understand and agree to comply with FHNC Electronic Communication Usage Procedures.  Also, you understand that the use of this computer and all related equipment, networks and network devices (including Internet access) may be monitored at any time and that unauthorized use may result in disciplinary action, up to and including termination. Copyright &copy; FTN Financial - All Rights Reserved</blockquote></font></p>
</body>
</html>
