﻿namespace FTN.MathLibrary
{
    public sealed class LeastSquareResultDataPoint
    {
        public decimal Predicted { get; private set; }

        /// <summary>
        /// Observed error
        /// </summary>
        public decimal Residual { get; private set; }

        public LeastSquareResultDataPoint(decimal predicted, decimal actual)
        {
            this.Predicted = predicted;
            this.Residual = actual - predicted;
        }
    }
}
