﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FTN.MathLibrary
{
    public static class General
    {
        public static decimal Mean(decimal[] values)
        {
            return Mean(values, true);
        }

        public static decimal Mean(decimal[] values, bool isPopulation)
        {
            if (isPopulation)
            {
                return values.Average();
            }
            else
            {
                return values.Sum() / (values.Count() - 1);
            }
        }

        public static DataPoint Mean(DataPoint[] points)
        {
            return Mean(points, true);
        }

        public static DataPoint Mean(DataPoint[] points, bool isPopulation)
        {
            DataPoint mean = new DataPoint(0.0m, 0.0m);
            int count = 0;

            for(; count < points.Length; count++)
            {
                mean.X += points[count].X;
                mean.Y += points[count].Y;
                count++;
            }

            if (isPopulation)
            {
                return new DataPoint(mean.X / count, mean.Y / count);
            }
            else
            {
                return new DataPoint(mean.X / (count - 1), mean.Y / (count - 1));
            }
        }

        public static DataPoint2 Mean(DataPoint2[] points)
        {
            return Mean(points, true);
        }

        public static DataPoint2 Mean(DataPoint2[] points, bool isPopulation)
        {
            var ivSum = new decimal[points.First().X.Length];
            var yMean = points.Select(p => p.Y).Average();
            var count = points.Count();

            for (var x = 0; x < ivSum.Length; x++)
            {
                for (var y = 0; y < points.Length; y++)
                {
                    ivSum[x] += points[y].X[x];
                }

                ivSum[x] = ivSum[x];
            }
            
            if (isPopulation)
            {
                for (var x = 0; x < ivSum.Length; x++)
                {
                    ivSum[x] /= count;
                }

                yMean /= count;
            }
            else
            {
                for (var x = 0; x < ivSum.Length; x++)
                {
                    ivSum[x] /= (count - 1);
                }

                yMean /= (count - 1);
            }

            return new DataPoint2(ivSum, yMean);
        }

        public static decimal Mode(decimal[] values)
        {
            if (values.Count() == 0)
            {
                return decimal.MinValue;
            }
            else if (values.Count() == 1)
            {
                return values.First();
            }

            var counter = values.ToDictionary(x => x, x => 0);
            for(var x = 0; x < values.Length; x++)
            {
                counter[values[x]]++;
            }

            var sorted = counter.OrderByDescending(x => x.Value).Select(y => new [] { y.Key, y.Value }).ToArray();
            if (sorted[0][1] == sorted[0][2])
            {
                return decimal.MinValue;
            }
            return sorted[0][0];
        }

        public static decimal Median(decimal[] values)
        {
            int count = values.Count();

            if (count == 0)
            {
                return decimal.MinValue;
            }
            else if (count == 1)
            {
                return values.First();
            }

            if ((count % 2) == 1)
            {
                var midPoint = count / 2;

                return (values.ElementAt(midPoint) + values.ElementAt(midPoint + 1)) / 2;
            }
            else
            {
                return values.ElementAt((count + 1) / 2);
            }            
        }
    }
}
