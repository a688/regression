﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//ⁿ₀₁₂₃₄₅ₑₓ∑ᾱ…ŷȳβᵦ

namespace FTN.MathLibrary
{
    /// <summary>
    /// 
    /// </summary>
    public static class Statistics
    {
        /// <summary>
        /// Pearson's r (measures correlation in the sample)
        /// </summary>
        /// <param name="points"></param>
        /// <returns></returns>
        public static decimal RSquared(DataPoint[] points)
        {
            var values = new[] { 0.0m, 0.0m, 0.0m };
            var mean = FTN.MathLibrary.General.Mean(points, true);

            for (var x = 0; x < points.Length; x++)
            {
                values[0] += (points[x].X - mean.X) * (points[x].Y - mean.Y);
                values[1] += (decimal)System.Math.Pow((double)(points[x].X - mean.X), 2);
                values[2] += (decimal)System.Math.Pow((double)(points[x].Y - mean.Y), 2);
            }

            return values[0] / (decimal)System.Math.Sqrt((double)(values[1] * values[2]));
        }

        /// <summary>
        /// Pearson's r (measures correlation in the sample)
        /// </summary>
        /// <param name="points"></param>
        /// <returns></returns>
        public static decimal RSquared(decimal[] points1, decimal[] points2)
        {
            var values = new [] { 0.0m, 0.0m, 0.0m };
            var mean1 = FTN.MathLibrary.General.Mean(points1, true);
            var mean2 = FTN.MathLibrary.General.Mean(points2, true);

            for (var x = 0; x < points1.Length; x++)
            {
                values[0] += (points1[x] - mean1) * (points2[x] - mean2);
                values[1] += (decimal)System.Math.Pow((double)(points1[x] - mean1), 2);
                values[2] += (decimal)System.Math.Pow((double)(points2[x] - mean2), 2);
            }

            return values[0] / (decimal)(System.Math.Sqrt((double)(values[1] * values[2])));
        }

        public static decimal Variance(decimal[] values, bool isPopulation)
        {
            var mean = FTN.MathLibrary.General.Mean(values, true);
            var squared = values.Select(x => (decimal)System.Math.Pow((double)(x - mean), 2.0)).ToArray();
            return FTN.MathLibrary.General.Mean(squared, isPopulation);
        }

        public static decimal StandardDeviation(decimal[] values, bool isPopulation)
        {
            return (decimal)System.Math.Sqrt((double)Variance(values, isPopulation));
        }

        public static decimal StandardError(decimal[] values)
        {
            return StandardDeviation(values, true) / (decimal)(System.Math.Sqrt(values.Count()));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="values"></param>
        /// <param name="isPopulation"></param>
        /// <remarks>If isPopulation = true, then we are calculating for β, if this is just a sample (isPopulation = false) then
        /// we are only calculating for a sample b. The equation we are using is yᵢ=β₀+β₁xᵢ+ϵᵢ (where ᵢ is the ith unit of the population).</remarks>
        public static LeastSquaresResultSet SimpleLeastSquares(DataPoint[] values, string ivName)
        {
            var ls = new LeastSquaresResultSet(new string[] { ivName }, values.Count());

            var ivMean = values.Select(x => x.X).Average();
            var ivSlope = FTN.MathLibrary.Geometry.Slope(values);
          
            // Calculate the average of the dependent
            ls.DependentAverage = values.Average(x => x.Y);

            // Calculate β₀ slope (average dependent value - (sum of the slope * average value for each independent value))
            var b0 = ls.DependentAverage - (ivSlope * ivMean);
            ls.Equation = @"y = " + b0.ToString() + " " + ivSlope.ToString() + ivMean.ToString();
            
            // Calculate the lineset/least squares
            for (var idx = 0; idx < values.Length; idx++)
            {
                ls.AddResult(new LeastSquareResultDataPoint(b0 + (ivSlope * values[idx].X), values[idx].Y), idx);
            }

            // ls.ResidualSumOfSquares should really be SSxi
            ls.StandardErrorX[0] = (decimal)System.Math.Sqrt((double)(ls.ResidualMeanSquares / (ls.ResidualSumOfSquares * (1 - ls.RSquared)))); 
            ls.StandardErrorY = StandardError(values.Select(x => x.Y).ToArray());

            return ls;
        }

        public static LeastSquaresResultSet MultipleLeastSquares(Matrix knownX, Matrix knownY, string[] ivNames)
        {
            if (knownX.Height != knownY.Height)
            {
                throw new ArgumentOutOfRangeException("The number of observations must be the same for both the x matrix and the y matrix");
            }
            else if (knownX.Width != ivNames.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(ivNames), "The number of x variables must be equal to the number of independent variable names");
            }

            var ls = new LeastSquaresResultSet(ivNames, knownX.Height);

            // Calculate the average (mean) for each of the x variables & the y variables
            var ivMean = new decimal[ivNames.Length];
            for (var y = 0; y < ivNames.Length; y++)
            {
                ivMean[y] = knownX.Column(y).Average();
            }
            var ȳ = knownY.Column(0).Average();

            // Calculate the slopes
            var β = Coefficients(knownX, knownY);
            ls.DependentAverage = β.GetValue(0, 0);

            // Build the equation in text format
            var equation = new System.Text.StringBuilder(β.GetValue(0, 0).ToString());
            for (var x = 1; x < β.Height; x++)
            {
                equation.Append(@" + " + β.GetValue(x, 0) + "X" + x.ToString());
            }
            ls.Equation = equation.ToString();

            // Calculate predicted values
            for (var row = 0; row < knownY.Height; row++)
            {
                var ŷ = 0.0m;

                for (var col = 0; col < knownX.Width; col++)
                {
                    ŷ += (knownX.GetValue(row, col) * β.GetValue(col, 0));
                }

                ls.AddResult(new LeastSquareResultDataPoint(ŷ, knownY.GetValue(row, 0)), row);
            }

            var H = Hat(knownX);
            var J = JMatrix(knownX.Height);
            var jByN = Matrix.Multiply(1.0m / J.Height, J);
            var yT = Matrix.Transpose(knownY);
            
            ls.RegressionSumOfSquares = CalculateSSr(knownY, yT, H, jByN);
            ls.ResidualSumOfSquares = CalculateSSe(knownY, yT, H);

            // Variance\Covariance matrix
            var C = Matrix.Multiply(ls.ResidualMeanSquares, MtmInv(knownX));
            for (int x = 0; x < C.Width; x++)
            {
                ls.StandardErrorX[x] = (decimal)System.Math.Sqrt((double)C.GetValue(x, x));
            }

            return ls;
        }

        /// <summary>
        /// (X´X)ˉ¹
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        public static Matrix MtmInv(Matrix m)
        {
            // X´
            var xT = MathLibrary.Matrix.Transpose(m);

            // X´X
            var xTx = MathLibrary.Matrix.Multiply(xT, m);

            // (X´X)ˉ¹
            return MathLibrary.Matrix.Inverse(xTx);
        }

        public static Matrix Hat(Matrix m)
        {
            // X´
            var xT = MathLibrary.Matrix.Transpose(m);

            // (X´X)ˉ¹
            var xTxINV = MtmInv(m);

            // X(X´X)ˉ¹
            var x_xTxINV = MathLibrary.Matrix.Multiply(m, xTxINV);

            // X(X´X)ˉ¹X´
            return MathLibrary.Matrix.Multiply(x_xTxINV, xT);
        }

        public static Matrix JMatrix(int size)
        {
            var j = new Matrix(size, size);

            for (int x = 0; x < size; x++)
            {
                for (int y = 0; y < size; y++)
                {
                    j.SetValue(x, y, 1);
                }
            }

            return j;
        }

        public static Matrix Coefficients(Matrix knownX, Matrix knownY)
        {
            var xT = MathLibrary.Matrix.Transpose(knownX);
            var xTy = MathLibrary.Matrix.Multiply(xT, knownY);
            return MathLibrary.Matrix.Multiply(MtmInv(knownX), xTy);
        }

        public static decimal CalculateSSr(Matrix y, Matrix yT, Matrix hat, Matrix jByN)
        {
            var inner = Matrix.Subtract(hat, jByN);
            return Matrix.Multiply(Matrix.Multiply(yT, inner), y).GetValue(0, 0);
        }

        public static decimal CalculateSSe(Matrix y, Matrix yT, Matrix hat)
        {
            var inner = Matrix.Subtract(Matrix.Identity(hat), hat);
            return Matrix.Multiply(Matrix.Multiply(yT, inner), y).GetValue(0, 0);
        }

        public static decimal TRatio(decimal r, int numberOfObservations)
        {
            return (r * (decimal)System.Math.Sqrt(numberOfObservations - 2)) / (decimal)(System.Math.Sqrt(1 - System.Math.Pow((double)r, 2.0)));
        }
    }
}