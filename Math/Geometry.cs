﻿using System.Collections.Generic;
using System.Linq;

namespace FTN.MathLibrary
{
    public static class Geometry
    {
        /// <summary>
        /// Calculates the slope of a line
        /// </summary>
        /// <param name="points"></param>
        /// <returns></returns>
        public static decimal Slope(DataPoint[] points)
        {
            var equation = new[] { 0.0m, 0.0m };
            var mean = FTN.MathLibrary.General.Mean(points, true);

            // Find the mean for the x and y data points for the slope equation

            /// Equation is: (variables with a hat mean average of that variable)
            ///      ∑(x - x̄)(y - ȳ)
            /// b = -----------------
            ///        ∑ (x - x̄)²
            for(var x = 0; x < points.Length; x++)
            {
                var xValue = (points[x].X - mean.X);

                equation[0] += xValue * (points[x].Y - mean.Y);
                equation[1] += (decimal)System.Math.Pow((double)xValue, 2);
            }

            return equation[0] / equation[1];
        }

        public static decimal[] Slope(DataPoint2[] points)
        {
            var slopes = new List<decimal>();
            var ivCount = points.First().X.Length;

            for (var x = 0; x < ivCount; x++)
            {
                var temp = new DataPoint[points.Length];
                for (var y = 0; y < points.Length; y++)
                {
                    temp[y] = points[y].ToDataPoint(x);
                }

                slopes.Add(Slope(temp));
            }

            return slopes.ToArray();
        }

        public static decimal YIntercept(DataPoint[] points)
        {
            var slope = Slope(points);
            DataPoint mean = FTN.MathLibrary.General.Mean(points, true);

            return mean.Y - (slope * mean.X);
        }

        public static decimal YIntercept(DataPoint2[] points)
        {
            var intercept = 0.0m;
            var ivCount = points.First().X.Length;

            for (var x = 0; x < ivCount; x++)
            {
                var temp = new DataPoint[points.Length];
                for (var y = 0; y < points.Length; y++)
                {
                    temp[y] = points[y].ToDataPoint(x);
                }

                intercept += (Slope(temp) * FTN.MathLibrary.General.Mean(temp, true).X);
            }

            return points.Select(p => p.Y).Average() - intercept;
        }   
    }
}
