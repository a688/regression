﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FTN.MathLibrary
{
    public sealed class DataPoint
    {
        /// <summary>
        /// Independent variable
        /// </summary>
        public decimal X { get; set; }

        /// <summary>
        /// Dependent variable
        /// </summary>
        public decimal Y { get; set; }

        public DataPoint(decimal x, decimal y)
        {
            this.X = x;
            this.Y = y;
        }
    }
}
