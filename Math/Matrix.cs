﻿using System;

namespace FTN.MathLibrary
{
    public sealed class Matrix
    {
        public int Width { get; private set; }

        public int Height { get; private set; }

        private decimal[][] _matrix;

        public Matrix(int height, int width)
        {
            this.Width = width;
            this.Height = height;

            _matrix = new decimal[height][];

            for (var x = 0; x < height; x++)
            {
                _matrix[x] = new decimal[width];
            }
        }

        public decimal GetValue(int row, int column)
        {
            return _matrix[row][column];
        }

        public void SetValue(int row, int column, decimal value)
        {
            _matrix[row][column] = value;
        }

        public void SetRowValues(int row, decimal[] values)
        {
            for (var x = 0; x < values.Length; x++)
            {
                _matrix[row][x] = values[x];
            }
        }

        public decimal[] Row(int x)
        {
            var row = new decimal[this.Width];

            for (var y = 0; y < this.Width; y++)
            {
                row[y] = _matrix[x][y];
            }

            return row;
        }

        public decimal[] Column(int x)
        {
            var col = new decimal[this.Height];

            for (var y = 0; y < this.Height; y++)
            {
                col[y] = _matrix[y][x];
            }
            
            return col;
        }

        public static Matrix Transpose(Matrix original)
        {
            var t = new Matrix(original.Width, original.Height);

            for (var x = 0; x < original.Height; x++)
            {
                for (var y = 0; y < original.Width; y++)
                {
                    t.SetValue(y, x, original.GetValue(x, y));
                }
            }

            return t;
        }

        public static Matrix Add(Matrix m1, Matrix m2)
        {
            if ((m1.Width != m2.Width) && (m1.Height != m2.Height))
            {
                throw new ArgumentOutOfRangeException("m1 and m2 must be the same size");
            }

            var result = new Matrix(m1.Height, m1.Width);

            for (var row = 0; row < m1.Height; row++)
            {
                for (var col = 0; col < m1.Width; col++)
                {
                    result.SetValue(row, col, m1.GetValue(row, col) + m2.GetValue(row, col));
                }
            }

            return result;
        }

        public static Matrix Subtract(Matrix m1, Matrix m2)
        {
            if ((m1.Width != m2.Width) && (m1.Height != m2.Height))
            {
                throw new ArgumentOutOfRangeException("m1 and m2 must be the same size");
            }

            var result = new Matrix(m1.Height, m1.Width);

            for (var row = 0; row < m1.Height;  row++)
            {
                for (var col = 0; col < m1.Width; col++)
                {
                    result.SetValue(row, col, m1.GetValue(row, col) - m2.GetValue(row, col));
                }
            }

            return result;
        }

        public static Matrix Multiply(Matrix m1, Matrix m2)
        {
            // Make sure we can multiply these together
            if (m1.Width != m2.Height)
            {
                throw new ArgumentOutOfRangeException("The width of m1 must equal the height of m2");
            }

            var p = new Matrix(m1.Height, m2.Width);

            for (var a = 0; a < m1.Height; a++)
            {
                var row = m1.Row(a);

                for (var b = 0; b < m2.Width; b++)
                {
                    var col = m2.Column(b);

                    var sum = 0.0m;

                    for (var z = 0; z < row.Length; z++)
                    {
                        sum += (row[z] * col[z]);
                    }

                    p.SetValue(a, b, sum);
                }
            }

            return p;
        }

        public static Matrix Multiply(decimal value, Matrix m)
        {
            for (var row = 0; row < m.Width; row++)
            {
                for (var col = 0; col < m.Height; col++)
                {
                    m.SetValue(row, col, m.GetValue(row, col) * value);
                }
            }

            return m;
        }

        public static Matrix Identity(Matrix m)
        {
            var ident = new Matrix(m.Width, m.Width);

            for (var x = 0; x < m.Width; x++)
            {
                ident.SetValue(x, x, 1);
            }

            return ident;
        }

        public static decimal Determinate(Matrix m)
        {
            return (m.GetValue(0, 0) * DeterminateSubMatrixValue(m, 0, 0)) -
                    (m.GetValue(0, 1) * DeterminateSubMatrixValue(m, 0, 1)) +
                    (m.GetValue(0, 2) * DeterminateSubMatrixValue(m, 0, 2));
        }

        public static decimal DeterminateSubMatrixValue(Matrix m, int row, int column)
        {
            var sm = SubMatrix(m, row, column);

            var total1 = 1.0m;
            var total2 = 1.0m;
            for (var x = 0; x < sm.Width; x++)
            {
                total1 *= sm.GetValue(x, x);
                total2 *= sm.GetValue(x, sm.Width - x - 1);
            }

            return total1 - total2;
        }

        public static Matrix Inverse(Matrix m1)
        {
            var det = Determinate(m1);
            var mT = Transpose(m1);
            var mTm = new Matrix(mT.Height, mT.Width);

            for (var x = 0; x < mTm.Width; x++)
            {
                for (var y = 0; y < mTm.Height; y++)
                {
                    mTm.SetValue(x, y, DeterminateSubMatrixValue(mT, x, y));
                }
            }

            for (var x = 0; x < mTm.Width; x++)
            {
                for (var y = 0; y < mTm.Height; y++)
                {
                    mTm.SetValue(x, y, mTm.GetValue(x, y) * (decimal)System.Math.Pow(-1, (x + 1) + (y + 1)));
                }
            }

            return Multiply(1 / det, mTm);
        }

        public static Matrix SubMatrix(Matrix m1, int parentRow, int parentColumn)
        {
            var smallM = new Matrix(m1.Height - 1, m1.Width - 1);
            int rowIdx = -1;
            int colIdx = 0;

            for (var row = 0; row < m1.Height; row++)
            {
                if (row != parentRow)
                {
                    rowIdx++;
                    colIdx = -1;

                    for (var col = 0; col < m1.Width; col++)
                    {
                        if (col != parentColumn)
                        {
                            colIdx++;
                            smallM.SetValue(rowIdx, colIdx, m1.GetValue(row, col));
                        }
                    }
                }
            }

            return smallM;
        }
    }
}
