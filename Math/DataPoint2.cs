﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FTN.MathLibrary
{
    public sealed class DataPoint2
    {
        /// <summary>
        /// Independent variables
        /// </summary>
        public decimal[] X { get; set; }
        
        /// <summary>
        /// Dependent variable
        /// </summary>
        public decimal Y { get; set; }

        public DataPoint2(decimal[] x, decimal y)
        {
            this.X = x;
            this.Y = y;
        }

        /// <summary>
        /// Convert the datapoint with multiple independent variables to a datapoint
        /// with a single independent variable
        /// </summary>
        /// <param name="xIndex"></param>
        /// <returns></returns>
        public DataPoint ToDataPoint(int xIndex)
        {
            if (xIndex < 0)
            {
                throw new ArgumentOutOfRangeException("xIndex", "This value cannot be less than 0");
            }
            else if (xIndex > this.X.GetUpperBound(0))
            {
                throw new ArgumentOutOfRangeException("xIndex", "This value cannot be greater than the number of x values in this set");
            }

            return new DataPoint(this.X[xIndex], this.Y);
        }
    }
}
