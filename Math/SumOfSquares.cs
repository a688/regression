﻿namespace FTN.MathLibrary
{
    public sealed class SumOfSquares
    {
        public decimal Regression { get; set; }

        public decimal Residual { get; set; }

        public decimal Total { 
            get
            {
                return this.Regression + this.Residual;
            }
        }

        public SumOfSquares()
        {
            this.Regression = 0.0m;
            this.Residual = 0.0m;
        }
    }
}
