﻿using System;
using System.Linq;

namespace FTN.MathLibrary
{
    public sealed class LeastSquaresResultSet
    {
        public int NumberOfObservations { get; private set; }

        public int NumberOfIV { get; private set; }

        public string[] IndependentVariables { get; private set; }

        public string Equation { get; set; }

        #region - Degrees of Freedom -

        public int RegressionDegreesOfFreedom
        {
            get
            {
                return this.NumberOfIV;
            }
        }

        public int ResidualDegreesOfFreedom
        {
            get
            {
                return this.NumberOfObservations - (this.NumberOfIV + 1);
            }
        }

        public int TotalDegreesOfFreedom
        {
            get
            {
                return this.RegressionDegreesOfFreedom + this.ResidualDegreesOfFreedom;
            }
        }

        #endregion

        #region - Sum of Squares -

        /// <summary>
        /// Explained variance
        /// </summary>
        public decimal RegressionSumOfSquares { get; set; }

        /// <summary>
        /// Observed errors/unexplained variance
        /// </summary>
        public decimal ResidualSumOfSquares { get; set; }

        public decimal TotalSumOfSquares
        {
            get
            {
                return this.RegressionSumOfSquares + this.ResidualSumOfSquares;
            }
        }

        #endregion

        #region - Mean Squares -

        public decimal RegressionMeanSquares
        {
            get
            {
                return this.RegressionSumOfSquares / this.RegressionDegreesOfFreedom;
            }
        }

        public decimal ResidualMeanSquares { 
            get
            {
                return this.ResidualSumOfSquares / this.ResidualDegreesOfFreedom;
            } 
        }

        #endregion

        #region - Additional Statistics -

        public decimal RegressionFRatio
        {
            get
            {
                return this.RegressionMeanSquares / this.ResidualMeanSquares;
            }
        }

        /// <summary>
        /// Proportion of explained variance
        /// </summary>
        /// <remarks>Always between 0 and 1, a value closer to 1 indicates a closer fit to the data)</remarks>
        public decimal FSignificance
        {
            get
            {
                return this.ResidualSumOfSquares / this.TotalSumOfSquares;
            }
        }

        public decimal MultipleR
        {
            get
            {
                return (decimal)System.Math.Sqrt((double)(this.RegressionSumOfSquares / this.TotalSumOfSquares));
            }
        }

        public decimal RSquared
        {
            get
            {
                return this.RegressionSumOfSquares / this.TotalSumOfSquares;
            }
        }

        public decimal AdjustedRSquared
        {
            get
            {
                return 1 - ((this.ResidualSumOfSquares / this.ResidualDegreesOfFreedom) / (this.TotalSumOfSquares / this.TotalDegreesOfFreedom));
            }
        }

        public decimal ResidualVariance // σ2
        {
            get
            {
                return (1 / this.ResidualDegreesOfFreedom) * this.ResidualSumOfSquares;
            }
        }

        /// <summary>
        /// Roughly the average absolute size of deviations of individuals from the sample regression line
        /// </summary>
        public decimal SampleStandardDeviation { 
            get 
            { 
                return (decimal)System.Math.Sqrt((double)this.ResidualMeanSquares); 
            } 
        }

        public decimal StandardErrorY { get; set; }

        public decimal[] StandardErrorX { get; internal set; }

        #endregion

        public decimal DependentAverage { get; set; }

        private LeastSquareResultDataPoint[] _results;

        public LeastSquareResultDataPoint[] Results
        {
            get
            {
                return _results;
            }
        }

        public LeastSquaresResultSet(string[] independentVariableNames, int numberOfObservations)
        {
            this.NumberOfObservations = numberOfObservations;
            _results = new LeastSquareResultDataPoint[numberOfObservations];

            this.IndependentVariables = new string[independentVariableNames.Length];
            Array.Copy(independentVariableNames, IndependentVariables, independentVariableNames.Length);

            this.NumberOfIV = independentVariableNames.Count();
            for(var n = 0;  n < independentVariableNames.Length; n++)
            {
                if (string.IsNullOrWhiteSpace(independentVariableNames[n]))
                {
                    this.NumberOfIV--;
                }
            }

            this.StandardErrorX = new decimal[independentVariableNames.Length];
        }

        public void AddResult(decimal predictedY, decimal actualY, int index)
        {
            AddResult(new LeastSquareResultDataPoint(predictedY, actualY), index);
        }
        
        public void AddResult(LeastSquareResultDataPoint value, int index)
        {
            //this.RegressionSumOfSquares += System.Math.Pow(value.Predicted - this.DependentAverage, 2.0);
            //this.ResidualSumOfSquares += System.Math.Pow(value.Residual, 2.0);      // Residual = actual - predicted

            _results[index] = value;
        }
    }
}
